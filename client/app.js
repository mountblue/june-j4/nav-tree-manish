$(document).ready(function () {
    let $seasons = $('#seasons');
    $.ajax({
        type: 'GET',
        url: '/seasons',
        success: function (resp) {
            console.log("success");
            resp.forEach(element => {
                $seasons.append('<li id='+element+''+' onclick="getTeams(this)">' + element+ '</li>'+'<br>');
            }); 
        },
        error: function (error) {
            console.log(error);
        }
    })
})

function getTeams(event){
    let season = event.id;
    let $seasons = $(event);
    $.ajax({
        type: 'GET',
        url: '/seasons/'+season,
        success: function (resp) {
            $('#'+season).empty();
            $('#'+season).text(season);
            console.log("success");
            resp.forEach(element => {
                $seasons.append('<div id='+element+''+' onclick="getPlayers(this)">' + element+ '</div>');
            }); 
        },
        error: function (error) {
            console.log(error);
        }
    })
}


function getPlayers(event){
    let id = event.id;
    let team  =$('#'+id).text();
    let season = event.parentNode.id;
    
    $.ajax({
        type: 'GET',
        url: '/seasons/'+season+'/teams/'+team,
        success: function (resp) {
            console.log("success");
            resp.forEach(element => {
                $('#'+season).append('<div id='+element+''+' onclick="getPlayers(this)">' + element+ '</div>');
                
                // $('#'+id).addClass("selected");
            }); 
            $('div').css(color,'red');
        },
        error: function (error) {
            console.log(error);
        }
    })
}