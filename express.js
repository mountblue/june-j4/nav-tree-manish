let express = require('express');
let path = require('path');
let file = path.resolve('matches.js');
let matches = require(file);
let app = express();

app.use(express.static(path.join(__dirname,'client')));

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.get('/', function (req, resp) {
    resp.render('index');
});

app.get('/seasons', function (req, resp, next) {
    matches.getMatchPerYear().then(function (seasons) {
        resp.json(seasons);
    })
})

app.get('/seasons/:season', function (req, resp, next) {
    let year = req.params.season;
    console.log(year);
    matches.getTeamsPerSeason(year).then(function (teams) {
        console.log(teams);
        resp.json(teams);
    })
})

app.get('/seasons/:season/teams/:team', function (req, resp, next) {
    let year = req.params.season;
    let team = req.params.team;
    console.log(year);
    console.log(team);
    matches.getPlayersPerTeam(year,team).then(function (teamNames) {
        console.log(teamNames);
        resp.json(teamNames);
    })
})

app.listen(4000, function () {
    console.log("server started");
})

// resp.render('index');
// resp.sendfil(__dirname+'/index.ejs')

// let express = require('express');
// let router = express.Router();
// let mongodb = require('mongodb');
// let url = "mongodb://localhost:27017";

// router.get('/',function(req,resp,next){
//     resp.render('index');
// });

// module.exports =  {
//     router : router
// }
