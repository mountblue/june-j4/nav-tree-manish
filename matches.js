const MongoClient = require("mongodb").MongoClient;
const url = "mongodb://127.0.0.1:27017";

function getMatchPerYear() {
    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (error, conn) {
            if (error) {
                reject(error.code);
            } else {
                let matchDB = conn.db("matchesdb");
                let matchCollection = matchDB.collection("matches");
                matchCollection.distinct(
                    "season",
                    (function (error, data) {
                        if (error) {
                            console.log(error);
                        }
                        else {
                            console.log(data);
                            resolve(data.sort());
                        }
                    })
                )
            }
        })
    })
}
// getMatchPerYear();

function getTeamsPerSeason(year) {

    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (error, conn) {
            if (error) {
                reject(error.code);
            } else {
                let matchDB = conn.db("matchesdb");
                let matchCollection = matchDB.collection("matches");
                matchCollection.distinct("team1",
                    {
                        season: Number(year)
                    },
                    (function (error, data) {
                        if (error) {
                            console.log(error);
                        }
                        else {
                            console.log(data);
                            resolve(data.sort());
                        }
                    }))
            }
        })
    })

}

// getTeamsPerSeason("2016");


function getPlayersPerTeam(year, team) {

    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (error, conn) {
            if (error) {
                reject(error.code);
            } else {
                let matchDB = conn.db("matchesdb");
                let matchCollection = matchDB.collection("matches");
                // let deliveriesCollection = matchDB.collection("deliveries");
                matchCollection.aggregate([{
                    $match: {
                        season: Number(year),
                        team1: team
                    }
                },
                {
                    $lookup: {
                        from: "deliveries",
                        localField: "id",
                        foreignField: "match_id",
                        as: "deliveriesNewDB"
                    }
                },
                {
                    $unwind: "$deliveriesNewDB"
                },

                {
                    $project: {
                        _id: 0,
                        battingTeam : '$deliveriesNewDB.batting_team',
                        player: '$deliveriesNewDB.batsman'
                    }
                },
                {
                    $match: {
                        battingTeam: team
                    }
                }
                ]).toArray(function (error, data) {
                    if (error) {

                    } else {
                        let playersArray = [];
                        data.forEach(element => {
                            if (!playersArray.includes(element.player)) playersArray.push(element.player)
                        })
                        resolve(playersArray);
                        console.log(playersArray);
                    }
                })
            }
        })
    })
}

getPlayersPerTeam(2017,"Sunrisers Hyderabad");

module.exports = {
    getMatchPerYear: getMatchPerYear,
    getTeamsPerSeason: getTeamsPerSeason,
    getPlayersPerTeam: getPlayersPerTeam
}